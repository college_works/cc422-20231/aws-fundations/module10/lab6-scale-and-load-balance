#!/bin/bash
#
# This script was written to automate the following task:
# - "Task 4: Verify that Load Balancing is Working"
# of "Lab 6: Scale and Load Balance Your Architecture" (AWS Cloud Foundation)

LOAD_BALANCER_NAME=LabELB
TARGET_GROUP_NAME=LabGroup

targetGroupArn=$(aws elbv2 describe-target-groups \
                  --names $TARGET_GROUP_NAME \
                  --query 'TargetGroups[].TargetGroupArn' \
                  --output text)

if [[ -z $targetGroupArn ]];then
  echo "[*] Target Group $TARGET_GROUP_NAME does not exist"
  exit 1
fi

echo "[*] Heath of instances registered to $TARGET_GROUP_NAME target group"
aws elbv2 describe-target-health \
  --target-group-arn $targetGroupArn \
  --query 'TargetHealthDescriptions[]'


lbDNSName=$(aws elbv2 describe-load-balancers \
              --query "LoadBalancers[?LoadBalancerName==\`$LOAD_BALANCER_NAME\`].DNSName" \
              --output text)
N=10
url="http://$lbDNSName"
echo "[*] Making $N requests to $url"
for i in $(seq $N);do
  instanceId=$(curl -s $url | grep -E "i-[a-z0-9]*" -o)
  echo "response from instance: $instanceId"
done
