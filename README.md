# Lab 6: Scale and Load Balance Your Architecture

Module: Module 10 - Auto Scaling and Monitoring

We will start with the following infrastructure:
![Initial Infraestructure](./images/init_infrastructure.png)

At the end of this lab, we must have the following infrastructure:
![Final Infraestructure](./images/final_infrastructure.png)