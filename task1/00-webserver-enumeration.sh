#!/bin/bash
#
# This enumerate attributes of the "Web Server 1" instance.
# Script 01-create_ami.sh will create an AMI from this instance.
#
# Requirements:
# - AWS CLI v2
# - jq (JSON parser)

INSTANCE_NAME='Web Server 1'

describeInstance=$(aws ec2 describe-instances \
                    --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                    --query 'Reservations[].Instances[]' | jq '.[]')

# AvailabilityZone
az=$(echo $describeInstance  \
      | jq '.Placement.AvailabilityZone')

echo "$INSTANCE_NAME AvailabilityZone: $az"

# AMI
instanceAMI=$(echo $describeInstance  \
      | jq -r '.ImageId')

echo "$INSTANCE_NAME AMI: $instanceAMI"
echo "AMI $instanceAMI description:"
aws ec2 describe-images \
  --image-ids $instanceAMI \
  --query 'Images[].Description'

# volume
volumeId=$(echo $describeInstance \
            | jq -r '.BlockDeviceMappings[].Ebs.VolumeId')

echo "$INSTANCE_NAME volume: $volumeId"
echo "Volume $volumeId description:"
aws ec2 describe-volumes \
  --volume-ids $volumeId \
  | grep -E "Size|VolumeType|Avai|Device"

# security group
sgId=$(echo $describeInstance \
        | jq -r '.SecurityGroups[].GroupId')

echo "$INSTANCE_NAME security group: $sgId"
echo "Security group $sgId ingress:"
aws ec2 describe-security-groups \
  --group-ids $sgId \
  --query 'SecurityGroups[].IpPermissions' \
  --no-paginate \
  --no-cli-pager

# connectivity
echo "Connectivity information of $INSTANCE_NAME instance"
echo $describeInstance | jq | grep -E 'PublicIpAddress|KeyName'