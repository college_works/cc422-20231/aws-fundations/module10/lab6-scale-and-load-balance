# References:
# - https://docs.aws.amazon.com/autoscaling/ec2/userguide/as-scaling-simple-step.html

# # Scale UP policy
resource "aws_autoscaling_policy" "example" {
  name                   = "LabScalingPolicy"
  autoscaling_group_name = aws_autoscaling_group.lab_auto_scaling_group.name
  policy_type            = "TargetTrackingScaling"
  
  target_tracking_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ASGAverageCPUUtilization"
    }

    target_value = 60.0
  }
}

# resource "aws_cloudwatch_metric_alarm" "scale_down" {
#   alarm_description   = "Monitors CPU utilization for ASG (down)"
#   alarm_actions       = [aws_autoscaling_policy.scale_down.arn]
#   alarm_name          = "asg_scale_down"
#   comparison_operator = "LessThanOrEqualToThreshold"
#   namespace           = "AWS/EC2"
#   metric_name         = "CPUUtilization"
#   threshold           = "60"
#   evaluation_periods  = "2"
#   period              = "120"
#   statistic           = "Average"

#   dimensions = {
#     AutoScalingGroupName = aws_autoscaling_group.lab_auto_scaling_group.name
#   }
# }

# # Scale UP policy
# resource "aws_autoscaling_policy" "scale_up" {
#   name                   = "LabScalingPolicy_up"
#   autoscaling_group_name = aws_autoscaling_group.lab_auto_scaling_group.name
#   adjustment_type        = "ChangeInCapacity"
#   scaling_adjustment     = 1
#   cooldown               = 120
# }

# resource "aws_cloudwatch_metric_alarm" "scale_up" {
#   alarm_description   = "Monitors CPU utilization for ASG (up)"
#   alarm_actions       = [aws_autoscaling_policy.scale_down.arn]
#   alarm_name          = "asg_scale_up"
#   comparison_operator = "GreaterThanOrEqualToThreshold"
#   namespace           = "AWS/EC2"
#   metric_name         = "CPUUtilization"
#   threshold           = "60"
#   evaluation_periods  = "2"
#   period              = "120"
#   statistic           = "Average"

#   dimensions = {
#     AutoScalingGroupName = aws_autoscaling_group.lab_auto_scaling_group.name
#   }
# }