## Launch Configuration

data "aws_ami" "web_server_ami" {
  filter {
    name = "name"
    values = ["WebServerAMI"]
  }
}

resource "aws_launch_configuration" "lab_lunch_config" {
  name              = "LabConfig"
  image_id          = data.aws_ami.web_server_ami.id # created AMI from Web Server 1 instance
  instance_type     = "t3.micro"
  enable_monitoring = true
  security_groups   = [data.aws_security_group.web_security_group.id]
  key_name          = "vockey"

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "lab_auto_scaling_group" {
  name                  = "Lab Auto Scaling Group"
  desired_capacity      = 2
  min_size              = 2
  max_size              = 6
  vpc_zone_identifier   = [data.aws_subnet.lab_private_subnet1.id,
                            data.aws_subnet.lab_private_subnet2.id]
  target_group_arns     = [aws_lb_target_group.lab_target_group.arn] 
  launch_configuration = aws_launch_configuration.lab_lunch_config.name

  lifecycle {
    create_before_destroy = true
  }

  tag {
    key                 = "Name"
    value               = "Lab Instance"
    propagate_at_launch = true
  }
}