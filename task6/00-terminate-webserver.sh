#!/bin/bash
#
# This script was written to automate the following task:
# - "Task 6: Terminate Web Server 1"
# of "Lab 6: Scale and Load Balance Your Architecture" (AWS Cloud Foundation)
#
# Requirements:
# - AWS CLI v2

INSTANCE_NAME='Web Server 1'

instanceId=$(aws ec2 describe-instances \
              --filters "Name=tag:Name,Values=$INSTANCE_NAME" \
                        "Name=instance-state-name,Values=running" \
              --query 'Reservations[].Instances[].InstanceId' \
              --output text)

echo "[*] Terminating $INSTANCE_NAME instance (InstanceId: $instanceId)"
aws ec2 terminate-instances \
  --instance-ids $instanceId


